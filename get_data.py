# Python 3.6.4
import os
import sys
import time
import json

import trademe
import into_csv

# Define variables
fields = ['region', 'address', 'cost_per_week', 'bedroom_num', 'bathroom_num',
'date_available', 'link']
current_dir_path = os.path.dirname(os.path.realpath(__file__))
data_dump_path = str('./data_dump/data_dump.{}.json'.format(time.time()))
csv_path = current_dir_path + '/compiled_data/house_data.csv'

search_url = sys.argv[1]


# Get trademe data
trademe_data = trademe.scrape_data(search_url)

# Dump data into json
into_csv.dump_to_json(data_dump_path, trademe_data)

# Turn data dump into CSV
into_csv.dump_to_csv(fields, data_dump_path, csv_path)

# Launch CSV
print('Launching csv in libreoffice.')
os.system('libreoffice --nologo --calc {}'.format(csv_path))
