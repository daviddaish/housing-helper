# Python 3.6.4
import os
import json
import operator
import csv

def dump_to_json(data_dump_file_name, data):
    # Dump data into json file
    print('Dumping data into json file...')
    with open(data_dump_file_name, 'w') as data_dump_file:
        json.dump(data, data_dump_file)


def dump_to_csv(wanted_fields, data_dump_path, csv_path):
    print('Converting data dump into csv...')

    # Open the data dump
    with open(data_dump_path, 'r') as data_dump_file:
        home_data = json.load(data_dump_file)

    # Write wanted columns into csv
    with open(csv_path, 'w') as csv_file:

        formatted_home_data = []
        for index, value in enumerate(home_data):
            row = {}
            for field in wanted_fields:
                row[field] = value[field]
            formatted_home_data.append(row)

        house_writer = csv.DictWriter(csv_file, wanted_fields)
        house_writer.writeheader()
        for home in formatted_home_data:
            house_writer.writerow(home)
