# Python 3.6.4
from bs4 import BeautifulSoup
import requests
import urllib.parse as urlparse
import re
import sys
import json
import time
import os

def get_all_listings(url):
    resulting_urls = []
    page_increment = 1

    while True:
        print('Checking if page number {} exists...'.format(page_increment))

        # Request the page
        page_request = requests.get(url + '&page=' + str(page_increment))

        # Get url crawler is eventually redirected to
        result_url = page_request.url

        # If the redirected to URL has already been visited, break
        if result_url in resulting_urls:
            break

        # Add the resuting url to the list of previous urls
        resulting_urls.append(result_url)

        # Increment the page number by one
        page_increment += 1

    return resulting_urls


# Gets useful information from home listing list pages
def get_homes(url):
    # Request and soup html
    search_request = requests.get(url)
    search_soup = BeautifulSoup(search_request.text, 'html.parser')

    # Get url scheme
    url_scheme = urlparse.urlparse(url).scheme

    # Get url netloc
    url_netloc = urlparse.urlparse(url).netloc

    # Get listings from html
    home_listings = search_soup('li', {'class': 'tmp-search-card-list-view'})

    # Get listing information
    house_list = []
    for listing in home_listings:
        house = {}

        if 'native-ad' in listing.get('class'):
            continue

        # Get house link
        house['link'] = url_scheme + '://' + url_netloc + listing.find('a').get('href')

        # Get house ID
        house['id'] = listing.find('a').get('id')

        # Find house bathrooms and bedrooms
        atributes_soups = listing.find(class_='tmp-search-card-list-view__attributes').find_all('span')
        atrib_value = []
        for atrib in atributes_soups:
            atrib_value.append(re.findall('\d+', atrib.string)[0])

        # Set house bedrooms
        house['bedroom_num'] = int(atrib_value[0])

        # Set house bathrooms
        house['bathroom_num'] = int(atrib_value[1])

        # Get address
        house['address'] = listing.find(class_='tmp-search-card-list-view__title').string

        # Get region
        house['region'] = listing.find(class_='tmp-search-card-list-view__subtitle').string

        # Set house cost per week
        house_string = listing.find(class_='tmp-search-card-list-view__price').string
        house_string = house_string.replace(',', '')
        house_string = house_string.replace('$', '')
        house_numbers = [x for x in house_string.split() if x.isdigit()]
        house['cost_per_week'] = int(house_numbers[0])

        # Get date available string
        available_date_strings = listing.find(class_='tmp-search-card-list-view__availability').string.split()

        # Set date avaible
        house['date_available'] = ' '.join(available_date_strings[2:])

        # Set misc house info
        house['misc_info'] = get_home_info(house['link'])

        # Append house to list of houses
        house_list.append(house)

    # Return the list of houses
    return house_list


# Gets more in depth information from a house page
def get_home_info(url):
    # Request and soup html
    house_request = requests.get(url)
    house_soup = BeautifulSoup(house_request.text, 'html.parser')

    house_info = {}

    house_info['table_info'] = {}

    # Get info in table
    info_table = house_soup.find(id='ListingAttributes')
    for row in info_table.find_all('tr'):
        title = ' '.join(row.find('th').stripped_strings)
        content = ' '.join(row.find('td').stripped_strings)

        house_info['table_info'][title] = content

    # Get description text
    house_info['text'] = ' '.join(house_soup.find(id='ListingDescription_ListingDescription').stripped_strings)

    # Return information
    return house_info

# Removes replica houses
def remove_replicas(houses):

    for house in houses:

        indexes_to_remove = []
        appeared_once = False

        for index, other_house in enumerate(houses):
            if house['id'] == other_house['id']:
                if appeared_once:
                    indexes_to_remove.append(index)
                else:
                    appeared_once = True

        for index in indexes_to_remove:
            del houses[index]

    return houses

# Performs the trademe search
def scrape_data(search_url_arg):

    # Get the trademe search url
    trademe_search = search_url_arg

    print('===')
    print('Starting Trademe scrape...')

    # Get all listing page urls
    page_urls = get_all_listings(trademe_search)

    # Get all home information
    homes = []
    for index, page in enumerate(page_urls):
        print('Getting home info from page {}...'.format(index + 1))
        homes = homes + get_homes(page)

    # Remove replicas
    print('Removing replicas from {} houses...'.format(str(len(homes))))
    dereplicated_homes = remove_replicas(homes)
    print('Reduced to {} houses...'.format(str(len(dereplicated_homes))))

    print('Trademe scrape complete.')
    print('===')

    return dereplicated_homes
